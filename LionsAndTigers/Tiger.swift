//
//  Tiger.swift
//  LionsAndTigers
//
//  Created by Neil on 29/10/2014.
//  Copyright (c) 2014 Coupaman. All rights reserved.
//

import Foundation
import UIKit

struct Tiger {
    var age = 0
    var name = ""
    var breed = ""
    var image = UIImage(named:"")
}