//
//  ViewController.swift
//  LionsAndTigers
//
//  Created by Neil on 29/10/2014.
//  Copyright (c) 2014 Coupaman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {


    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var breedLabel: UILabel!
    @IBOutlet weak var myImageView: UIImageView!
    
    var myTigers:[Tiger] = []
    var currentTiger = -1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        var myTiger = Tiger()
        myTiger.name = "Tigger"
        myTiger.breed = "Bengal"
        myTiger.age = 3
        myTiger.image = UIImage(named: "BengalTiger.jpg")

        var secondTiger = Tiger()
        secondTiger.name = "Tigress"
        secondTiger.breed = "Indochinese Tiger"
        secondTiger.age = 2
        secondTiger.image = UIImage(named: "IndochineseTiger.jpg")

        var thirdTiger = Tiger()
        thirdTiger.name = "Jacob"
        thirdTiger.breed = "Malayan Tiger"
        thirdTiger.age = 4
        thirdTiger.image = UIImage(named: "MalayanTiger.jpg")

        var fourthTiger = Tiger()
        fourthTiger.name = "Spar"
        fourthTiger.breed = "Siberian Tiger"
        fourthTiger.age = 5
        fourthTiger.image = UIImage(named: "SiberianTiger.jpg")

        myTigers += [myTiger, secondTiger, thirdTiger, fourthTiger]

        // Simulate an initial button press to save us having to preload
        // the initial tiger settings. This also gives us a random first tiger.
        nextBarButtonPressed(UIBarButtonItem())
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func nextBarButtonPressed(sender: UIBarButtonItem) {

        var newTiger = 0
        do {
            
            newTiger = Int(arc4random_uniform(UInt32(myTigers.count)))

        } while currentTiger == newTiger

        let tiger = myTigers[newTiger]
 
//        Standard code to change to new Tiger. Superceded by UIView.transitionWithView below
//        myImageView.image = tiger.image
//        nameLabel.text = tiger.name
//        ageLabel.text = "\(tiger.age)"
//        breedLabel.text = tiger.breed
//        
        currentTiger = newTiger
        
        // Advanced code to animate the transitions between views (in this case the two views are the same view)
        UIView.transitionWithView(self.view, duration: 1, options: UIViewAnimationOptions.TransitionCrossDissolve,
            animations: {
                self.myImageView.image = tiger.image
                self.nameLabel.text = tiger.name
                self.ageLabel.text = "\(tiger.age)"
                self.breedLabel.text = tiger.breed
          
            },
            completion: {
                (finished:Bool)->() in
            })
    }

}

